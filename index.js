var marked = require('marked');

marked.setOptions({
  sanitize: true,
});
console.log('If you see `javascript&#58this;alert(1&#41;` output on the following line, then marked is vulnerable');
console.log(marked('[Gotcha](javascript&#58this;alert(1&#41;)'));
